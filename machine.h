/* machine.h */

#ifndef MACHINE_H
#define MACHINE_H

#include "handy_funcs.h"
#include "simulator.h"
#include "program.h"

// Note: Commutative ops are commutative in operands 0 and 1.
typedef struct InstructionSet
{
   OpSim *proc;                 // Procedure for simulating the op.
   int  numopnds;               // Number of operands, 1 to 3.
   int  commutative;            // 1 if opnds 0 and 1 commutative.
   int  opndstart[MAX_OPRNDS];  // Starting reg no. for each operand.
   const char *mnemonic;        // Name of op, for printing.
//   const char *fun_name;      // Function name, for printing.
//   const char *op_name;       // Operator name, for printing.
}
InstructionSet;

// The instruction set:
static const InstructionSet instrSet[] = {
    {neg,    1, 0, {RX,  0}, "NEG"   },  // Negate.
    {_not,   1, 0, {RX,  0}, "NOT"   },  // One's-complement.
    {pop,    1, 0, {RX,  0}, "POP"   },  // Population count.
    {nlz,    1, 0, {RX,  0}, "NLZ"   },  // Num leading 0's.
//    {rev,    1, 0, {RX,  0}, "rev"    },  // Bit reversal.
//    {revb,   1, 0, {RX,  0}, "revb"   }, // Byte reversal.
    {add,    2, 1, {RX,  2}, "ADD"   },  // Add.
    {sub,    2, 0, { 2,  2}, "SUB"   },  // Subtract.
//    {rsb,    2, 0, { 2,  2}, "rsb"   },  // Reverse subtract.
    {mul,    2, 1, {RX,  3}, "MUL"   },  // Multiply.
//    {_div,   2, 0, { 1,  3}, "DIV"   },  // Divide signed.
//    {_divu,  2, 0, { 1,  1}, "divu"   },  // Divide unsigned.
//    {_mod,   2, 0, { 1,  3}, "MOD"   },  // Modulus signed.
//    {_modu,  2, 0, { 1,  1}, "modu"   },  // Modulus unsigned.
    {_and,   2, 1, {RX,  2}, "AND"   },  // AND.
    {_or,    2, 1, {RX,  2},  "OR"    },  // OR.
    {_xor,   2, 1, {RX,  2}, "XOR"   },  // XOR.
    {_bic,   2, 1, {RX,  2}, "BIC"   },  // AND-NOT / bitwise clear.
//    {rotl,   2, 0, { 1,NIM}, "rotl"   },  // Rotate shift left.
//    {rotr,   2, 0, { 1,NIM}, "rotr"   },  // Rotate shift right.
    {shl,    2, 0, { 1,NIM}, "SHL"   },  // Shift left.
//    {shr,    2, 0, { 1,NIM}, "SRU"   },  // Shift right.
    {shrs,   2, 0, { 3,NIM}, "SHR"   },  // Shift right signed.
//    {cmpeq,  2, 1, {RX,  0}, "cmpeq"  },  // Compare equal.
//    {cmplt,  2, 0, { 0,  0}, "cmplt"  },  // Compare less than.
//    {cmpltu, 2, 0, { 1,  1}, "cmpltu" },  // Compare less than unsigned.
};

#define NUM_INSNS_IN_SET ARR_LENGTH(instrSet)

#endif /* MACHINE_H */

