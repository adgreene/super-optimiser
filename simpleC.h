#ifndef simpleC_H
#define simpleC_H

#define VARIABLE(v)  { enum v { E }; }
#define CONST_INT(i) { enum { E = i }; }

/* might use these later
VARIABLE(src1) \
VARIABLE(src2) \
*/

#define ASSIGN(variable, value) \
VARIABLE(variable) \
CONST_INT(value) \
variable = value;


// Arithmetic operations

#define ADD(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 + src2;


#define SUB(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 - src2;


#define MUL(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 * src2;


#define DIV(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 / src2;


#define MOD(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 % src2;


// Bitwise operations

#define POP(dest,src) \
VARIABLE(dest) \
dest = __builtin_popcount(src);


#define NLZ(dest,src) \
VARIABLE(dest) \
dest = __builtin_clz(src);


#define NEG(dest,src) \
VARIABLE(dest) \
dest = -(src);


#define NOT(dest,src) \
VARIABLE(dest) \
dest = ~(src);


#define REVB(dest,src) \
VARIABLE(dest) \
dest = __builtin_bswap32(src);


#define AND(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 & src2;


#define OR(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 | src2;


#define XOR(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 ^ src2;


#define BIC(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 & ~src2;


// Bit Shift operations

#define SHL(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 << src2;


#define SHR(dest,src1,src2) \
VARIABLE(dest) \
dest = src1 >> src2;


#endif // simpleC_H