#include <stdio.h>
#include <stdbool.h>
#include "config.h"
//#include "search_set.h"
#include "simpleC.h"
#include "simulator.h"
#include "machine.h"

typedef struct Instruction {
   int operation;
   int operand[3];
} Instruction;


typedef struct Program {
   int length;
   Instruction instruction[4];
} Program;

static inline void instr_set(Instruction *const instr, int op) {
    (*instr).operation  = op;
    (*instr).operand[0] = instrSet[op].opndstart[0];
    (*instr).operand[1] = instrSet[op].opndstart[1];
    (*instr).operand[2] = instrSet[op].opndstart[2];
    return;
}

static inline void instr_init(Instruction *const instr) {
    instr_set(instr, 0);
    return;
}

int userfun1(int x, int y) {
    return ((unsigned long long)x + (unsigned long long)y) >> 1;
}

int userfun2(int rx, int ry) {
    int r1,r2,r3,r4;
//    r1 = ry ^ rx;
//    r2 = ry & rx;
//    r3 = r1 >> 1;
//    r4 = r3 + r2;
    XOR( r1,ry,rx );
    AND( r2,ry,rx );
    SHR( r3,r1, 1 );
    ADD( r4,r3,r2 );
    return r4;
}

int main () {
    int a = 5;
    int b = 11;
    int c = 17;
    
//    printf("%d, %d\n", (30 >> 2), (-30 >> 2));
    
//    printf("%d, %d\n", userfun1(105, 720), userfun2(105, 720));

//    printf("%d\n", __builtin_popcount(b));

    Instruction instr[2];
    instr_init(&instr[0]);
    instr_incr(&instr[0]);
    instr_init(&instr[1]);
    instr_incr(&instr[1]);
    instr_incr(&instr[1]);
    instr_incr(&instr[1]);
    printf("%d %d %d %d\n", instr[1].operation, instr[1].operand[0], instr[1].operand[1], instr[1].operand[2]);
    
    return b;
}