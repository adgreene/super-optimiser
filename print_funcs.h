/* print_funcs.h */
#ifndef PRINT_FUNCS_H
#define PRINT_FUNCS_H

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

static inline void print_to_console(char *format, ...) {
   va_list args;
   
   va_start(args, format);
   vprintf(format, args);
   va_end(args);
}

static inline void print_to_file(FILE *out, char *format, ...) {
   va_list args;
   
   va_start(args, format);
   vfprintf(out, format, args);
   va_end(args);
}


#endif