/* program.c */
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "handy_funcs.h"
#include "config.h"
#include "print_funcs.h"
#include "simpleC.h"
#include "simulator.h"

#include "program.h"
#include "machine.h"

#include INC

//static int   gl_memory[NIM + NSHIM + NARGS + MAX_INSTRS] = {IMMEDS, SHIMMEDS};
static   int   gl_memory[MEMORY_SIZE] = {IMMEDS, SHIMMEDS};
static Program gl_program;

static const int trialx[] = TRIAL;
#if NARGS >= 2
static const int trialy[] = TRIAL;
#endif
#if NARGS >= 3
static const int trialz[] = TRIAL;
#endif

#define NTRIALX ARR_LENGTH(trialx)
#define NTRIALY ARR_LENGTH(trialy)
#define NTRIALZ ARR_LENGTH(trialz)

#if NARGS == 1
static int correct_result[NTRIALX];
#elif NARGS == 2
static int correct_result[NTRIALX][NTRIALY];
#elif NARGS == 3
static int correct_result[NTRIALX][NTRIALY][NTRIALZ];
#endif


int unacceptable;

uint64_t progCount;



static inline void instr_set(Instruction* const instr, int op) {
    instr->opcode  = op;
    instr->operand[0] = instrSet[op].opndstart[0];
    instr->operand[1] = instrSet[op].opndstart[1];
    return;
}


static inline void instr_init(Instruction* const instr) {
    instr->opcode  = 0;
    instr->operand[0] = instrSet[0].opndstart[0];
    instr->operand[1] = instrSet[0].opndstart[1];
    return;
}


static inline void global_prgrm_layer_set(const int startInstr) {
    int i, o, maxOperand;
    const int numInstructions = gl_program.length;
    
    for (i = startInstr; i < numInstructions; i++) {
        maxOperand = RI0-1;
        gl_program.layer[i] = 0;
        for (o = 0; o < MAX_OPRNDS; o++) {
            if (gl_program.instruction[i].operand[o] > maxOperand) {
                maxOperand = gl_program.instruction[i].operand[o];
                gl_program.layer[i] = gl_program.layer[maxOperand - RI0] + 1;
            }
        }
    }
    return;
}


static inline int prgrm_init(Program* const prog, int numInstructions) {
    if (numInstructions > MAX_INSTRS) {
        printf("Number of instructions has exceeded the maximum allowed.\n");
        return ERROR;
    }
    else {
        Program pr;
        pr.length = numInstructions;
        for (int i = 0; i < MAX_INSTRS; i++) {
//            instr_init(&pr.instruction[i]);
            pr.instruction[i].opcode  = 0;
            pr.instruction[i].operand[0] = instrSet[0].opndstart[0];
            pr.instruction[i].operand[1] = instrSet[0].opndstart[1];
        }
        *prog = pr;
        return OK;
    }
}
static inline int global_prgrm_init(int numInstructions) {
    if (numInstructions > MAX_INSTRS) {
        printf("Number of instructions has exceeded the maximum allowed.\n");
        return ERROR;
    }
    else {
        gl_program.length = numInstructions;
        for (int i = 0; i < MAX_INSTRS; i++) {
//            instr_init(&gl_program.instruction[i]);
            gl_program.instruction[i].opcode  = 0;
            gl_program.instruction[i].operand[0] = instrSet[0].opndstart[0];
            gl_program.instruction[i].operand[1] = instrSet[0].opndstart[1];
//          for (int o = 0; o < MAX_OPRNDS; o++) {
//              gl_program.instruction[i].operand[o] = instrSet[0].opndstart[o];
//          }
//            gl_program.layer[i] = 0;
        }
        global_prgrm_layer_set(0);
        return OK;
    }
}


static inline void prgrm_print(const Program prog) {
    int i, j, k, opndj;
    
    int numInstructions = prog.length;
//    int gl_memory[NIM + NSHIM + NARGS + MAX_INSTRS] = {IMMEDS, SHIMMEDS};

    for (i = 0; i < numInstructions; i++) {
        k = prog.instruction[i].opcode;
//        print_to_console("L%d", prog.layer[i]);
        print_to_console("    %3s( r%d,", instrSet[k].mnemonic, i);
        for (j = 0; j < instrSet[k].numopnds; j++) {
            opndj = prog.instruction[i].operand[j];
            // immediates
            if (opndj < NIM) {
                opndj = gl_memory[opndj];
                if (opndj >= -31 && opndj <= 31) print_to_console("%d", opndj);
                else print_to_console("0x%X", opndj);
            }
            // shift immediates
            else if (opndj < RX) {
                opndj = gl_memory[opndj];
                print_to_console("%d", opndj);
            }
            // inputs
            else if (opndj == RX) print_to_console("x");
#if NARGS >= 2
            else if (opndj == RY) print_to_console("y");
#endif
#if NARGS >= 3
            else if (opndj == RZ) print_to_console("z");
#endif
            // Results from previous instructions
            else print_to_console("r%d", opndj - RI0);
            if (j < instrSet[k].numopnds - 1) print_to_console(",");
        }
        print_to_console(" );");
        print_to_console("\n");
    } // end for i
    print_to_console("    return r%d;\n", numInstructions-1);
   
    printf("Successfully printed program.\n\n");

}


static inline int prgrm_incr(Program* const prog) {

    Program pr = *prog;
    const int numInstructions = pr.length;
    
    int i, j, k, opndj, nopnds, lastModified, incremented;
    
    do {
        incremented = FALSE;
        lastModified = -1;
        i = numInstructions - 1;
        
        while (i >= 0 && !incremented) {
            Instruction instr = pr.instruction[i];
            k = instr.opcode;
            nopnds = instrSet[k].numopnds;
            j = 0;
            while (j < nopnds && !incremented) {
//                printf("Operand:%d.\n\n", j);
                opndj = instr.operand[j];
        
                if (opndj < NIM - 1) {         // If ordinary imm. and not last,
                    instr.operand[j] += 1;        // increment the operand.
                    incremented = TRUE;
//                    printf("Imm Operand:%d.\n\n", j);
                }
                else if (opndj == NIM - 1) {   // If last ordinary imm. operand,
                    instr.operand[j] = RX;        // skip to first register.
                    incremented = TRUE;
//                    printf("Last Imm Operand:%d.\n\n", j);
                }
                else if (opndj < i + RI0 - 1) {// If shift imm. or gl_memory and not
                    instr.operand[j] += 1;        // last, increment the operand.
                    incremented = TRUE;
//                    printf("Shift, Reg Operand:%d.\n\n", j);
                }
                else {
                    // We're at the end for opnd j.
                    // Reset it and increment next operand to its right.
                    instr.operand[j] = instrSet[k].opndstart[j];
                    incremented = FALSE;
//                    printf("End of Operand:%d.\n\n", j);
                    j++;
                }
            } // stop trying to increment the operands
        
            // Have we incremented all the operands?
            if (j == 0) {
                lastModified = i;
            }
            else if (j < nopnds) {
                
                lastModified = i;
            }
            // If so have we incremented the operator to the max?
            else if (k < NUM_INSNS_IN_SET - 1) {
                k = k + 1;             // Increment to next instrSet instruction.
                instr_set(&instr, k);
                incremented = TRUE;
                lastModified = i;
            }
            // If so we need to reset this instruction and tell the caller
            else {
                // Cannot increment to next instrSet insn.  Reset it to the first
                // instrSet insn and look at next insn down in the program.  Furthermore,
                // if the insn being reset is the last insn in the program, make
                // its first opnd pick up the previous insn's result.
    
                instr_init(&instr);
                incremented = FALSE;
            }
            
            // Do a bunch of small fixes to operands like fixing commutative
            // instructions, making sure the last instruction always points
            // to the previous one
            if (j != 0)
//            if (0 != 0)
            {
                // Check to see any redundant instructions, like in commutative operations
                int rs, rt;
    
                // If this is the last instruction, then make sure that
                // it uses the result from the previous instruction
                if (i == numInstructions-1) {         // If this is the last insn:
                    rs = RI0 + numInstructions-2;      // Second from last gl_memory.
//                    printf("%d, %d\n", i, rs);
                    if (instr.operand[1] != rs) {
                        instr.operand[0]  = rs;
//                        lastModified = -1;
                    }
                    
                    Instruction prevInstr = pr.instruction[i-1];
                    rt = rs - 1;              // Third from last gl_memory.
                    if( prevInstr.operand[0] != rt && 
                        prevInstr.operand[1] != rt && instr.operand[1] != rt &&
                        rt >= RI0) {
                
                        // The last instruction needs to reference rt.
                
                        if (instr.operand[0] < rt) instr.operand[0] = rt;
                        else if (instrSet[k].numopnds > 1) instr.operand[1] = rt;
                
                        // else (unary op), forget it.
                    }
                }
    
                if (instrSet[k].commutative) {
                    if (instr.operand[0] < instr.operand[1])
                        instr.operand[0] = instr.operand[1];
                                            // No need to do next check, as opnd[0]
                }                           // is always a gl_memory containing a variable.
                // Instructions made up entirely of immediates aren't likely to be of any use.
                else if (i != numInstructions - 1) {
                    if (    instr.operand[0] < RX &&
                            instr.operand[1] < RX) {
//                        if (instrSet[k].commutative) abort();
                        instr.operand[0] = RX;
                    }
                }
            }
            
            pr.instruction[i] = instr;
            i--;
        } // end for i
    } while (0 != 0);
    (*prog) = pr;
    progCount++;
    return lastModified;
}
static inline int global_prgrm_incr() {

    const int numInstructions = gl_program.length;
    
    int i, j, k, opndj, nopnds, lastModified, incremented;
    
    incremented = FALSE;
    lastModified = -1;
    i = numInstructions - 1;
    
    while (i >= 0 && !incremented) {
        k = gl_program.instruction[i].opcode;
        nopnds = instrSet[k].numopnds;
        j = 0;
        while (j < nopnds && !incremented) {
//            printf("Operand:%d.\n\n", j);
            opndj = gl_program.instruction[i].operand[j];
    
            if (opndj < NIM - 1) {                          // If ordinary imm. and not last,
                gl_program.instruction[i].operand[j] += 1;        // increment the operand.
                incremented = TRUE;
//                printf("Imm Operand:%d.\n\n", j);
            }
            else if (opndj == NIM - 1) {                    // If last ordinary imm. operand,
                gl_program.instruction[i].operand[j] = RX;        // skip to first register.
                incremented = TRUE;
//                printf("Last Imm Operand:%d.\n\n", j);
            }
            else if (opndj < i + RI0 - 1) {                 // If shift imm. or result of instr and
                gl_program.instruction[i].operand[j] += 1;  // not last, increment the operand.
                incremented = TRUE;
//                printf("Shift, Reg Operand:%d.\n\n", j);
            }
            else {
                // We're at the end for opnd j.
                // Reset it and increment next operand to its right.
                gl_program.instruction[i].operand[j] = instrSet[k].opndstart[j];
                incremented = FALSE;
//                printf("End of Operand:%d.\n\n", j);
                j++;
            }
        } // stop trying to increment the operands
    
        // Have we incremented all the operands?
        if (j < nopnds) {
            lastModified = i;
        }
        // If so have we incremented the operator to the max?
        else if (k < NUM_INSNS_IN_SET - 1) {
            k = k + 1;             // Increment to next instrSet instruction.
            //instr_set(&instr, k);
            gl_program.instruction[i].opcode  = k;
            gl_program.instruction[i].operand[0] = instrSet[k].opndstart[0];
            gl_program.instruction[i].operand[1] = instrSet[k].opndstart[1];
            incremented = TRUE;
            lastModified = i;
        }
        // If so we need to reset this instruction and tell the caller
        else {
            // Cannot increment to next instrSet insn.  Reset it to the first
            // instrSet insn and look at next insn down in the program.  Furthermore,
            // if the insn being reset is the last insn in the program, make
            // its first opnd pick up the previous insn's result.
    
            //instr_init(&instr);
            gl_program.instruction[i].opcode  = 0;
            gl_program.instruction[i].operand[0] = instrSet[0].opndstart[0];
            gl_program.instruction[i].operand[1] = instrSet[0].opndstart[1];
            incremented = FALSE;
        }
        
        
        if (i != numInstructions-1 && incremented) {
            const int startInstr = 0;
            global_prgrm_layer_set(startInstr);
            
            if (0) {
                printf("~~~~~~~~~~~~~~i=%d, debug2~~~~~~~~~~~~~~\n\n", startInstr);
                prgrm_print(gl_program);
                printf("~~~~~~~~~~~~~~end of debug2~~~~~~~~~~~~~~\n\n");
            }
            
            for(int i2 = startInstr+1; i2 < numInstructions; i2++) {
                
                // First, if the current instruction is below the current layer
                // change operand 0 to the result of the first instruction from
                // the previous layer in order to move it up to the current layer
                if (gl_program.layer[i2] < gl_program.layer[i2-1]) {
                    // Find the first instruction from the previous Layer
                    int prevLayerInstr = 0;
                    while (gl_program.layer[prevLayerInstr] != gl_program.layer[i2-1]-1) {
                        prevLayerInstr++;
                    }
//                    // Error check, very unlikely to happen, should probably delete this soon
//                    if (prevLayerInstr >= MAX_INSTRS) {
//                        printf("Oh no! Very unlikely error. prevLayerInstr=%d.\n", prevLayerInstr);
//                        return -1;
//                    }
                    gl_program.instruction[i2].operand[0] = RI0 + prevLayerInstr;
                    gl_program.layer[i2] = gl_program.layer[i2-1];
//                    global_prgrm_layer_set(i2);
                }

                // Next, if the current instruction is on the current layer
                // check to see if the instructions are in the canonical order.
                // This means comparing the operation and operands of both instructions.
                // If they are not in canonical order then the only way to fix this
                // without skipping over potential solutions is to move the current
                // instruction to the next layer.
                if (gl_program.layer[i2] == gl_program.layer[i2-1]) {

                    // NOTE: If the instructions are equal(same everything) then a redundancy exists.
                    // Technically they are in canonical form, but the canonForm flag is set to false anyway.

                    const int instrValue      = gl_program.instruction[i2].opcode    * MEMORY_SIZE*MEMORY_SIZE
                                              + gl_program.instruction[i2].operand[1]   * MEMORY_SIZE
                                              + gl_program.instruction[i2].operand[0];

                    const int lastInstrValue  = gl_program.instruction[i2-1].opcode  * MEMORY_SIZE*MEMORY_SIZE 
                                              + gl_program.instruction[i2-1].operand[1] * MEMORY_SIZE
                                              + gl_program.instruction[i2-1].operand[0];

                    if (instrValue <= lastInstrValue) {
                        if (gl_program.instruction[i2].opcode == gl_program.instruction[i2-1].opcode) {
                            gl_program.instruction[i2] = gl_program.instruction[i2-1];
                            gl_program.instruction[i2].operand[0]++;
//                            gl_program.instruction[i2].operand[0] = gl_program.instruction[i2-1].operand[0]+1;
                        }
                        else {
                            // Find the first instruction from the previous Layer
                            int currLayerInstr = 0;
                            while (gl_program.layer[currLayerInstr] != gl_program.layer[i2-1]) {
                                currLayerInstr++;
                            }
                            gl_program.instruction[i2].operand[0] = RI0 + currLayerInstr;
//                          global_prgrm_layer_set(i2);
                        }
                    }
                }
            }
        }
        
        // Do a bunch of small fixes to operands like fixing commutative
        // instructions, making sure the last instruction always points
        // to the previous one
        if (j != 0 && 1) {
//                printf("i=%d, incr=%d, debug1\n\n", i, incremented);
            // Check to see any redundant instructions, like in commutative operations
            int rs, rt;
    
            // If this is the last instruction, then make sure that
            // it uses the result from the previous instruction
            if (i == numInstructions-1) {         // If this is the last insn:
                rs = RI0 + numInstructions-2;      // Second from last gl_memory.
//                printf("%d, %d\n", i, rs);
                if (gl_program.instruction[i].operand[1] != rs) {
                    gl_program.instruction[i].operand[0]  = rs;
                }
                
                Instruction prevInstr = gl_program.instruction[i-1];
                rt = rs - 1;              // Third from last gl_memory.
                if( prevInstr.operand[0] != rt && 
                    prevInstr.operand[1] != rt && gl_program.instruction[i].operand[1] != rt &&
                    rt >= RI0) {
                    // The last instruction needs to reference rt.
            
                    if (gl_program.instruction[i].operand[0] < rt) gl_program.instruction[i].operand[0] = rt;
                    else if (instrSet[k].numopnds > 1)             gl_program.instruction[i].operand[1] = rt;
                //  else (unary op), forget it.
                }
            }
    
            if (instrSet[k].commutative) {
                if (gl_program.instruction[i].operand[0] < gl_program.instruction[i].operand[1])
                    gl_program.instruction[i].operand[0] = gl_program.instruction[i].operand[1];
                                        // No need to do next check, as opnd[0]
            }                           // is always a register containing a variable.
            // Instructions made up entirely of immediates aren't likely to be of any use.
            else if (i != numInstructions - 1) {
                if (    gl_program.instruction[i].operand[0] < RX &&
                        gl_program.instruction[i].operand[1] < RX) {
                    gl_program.instruction[i].operand[0] = RX;
                }
            }
        }
        i--;
    } // end for i
    progCount++;
    return lastModified;
}




//static inline int instr_simulate(const Instruction instr, int* const gl_memory) {
static inline int instr_simulate(const Instruction instr) {

    int arg0, arg1, arg2;

    arg0 = gl_memory[instr.operand[0]];
    arg1 = gl_memory[instr.operand[1]];
//  arg2 = gl_memory[instr.operand[2]];
    arg2 = 0;

    int result = (*instrSet[instr.opcode].proc)(arg0, arg1, arg2);
    return result;
}




//static inline int prgrm_simulate(const Program prog, const int startInstr, int* const gl_memory) {
static inline int prgrm_simulate(const Program prog, const int startInstr) {
    
    if (startInstr > prog.length) {
        printf("Program simulation has gone horribly wrong. startInstr: %d\n", startInstr);
        return ERROR;
    }
    
    int i = startInstr;
    int numInstructions = prog.length;
    
    do {
        Instruction instr = prog.instruction[i];
//        gl_memory[RI0 + i] = instr_simulate(instr, &gl_memory[0]);

        gl_memory[RI0 + i] = (*instrSet[instr.opcode].proc)(gl_memory[instr.operand[0]], gl_memory[instr.operand[1]], 0);
        
//        print_to_console("%4s r%d, % d, % d, % d ) ===> %4s r%d, % d, % d, % d )\n",
//                            instrSet[instr.opcode].mnemonic, i + 1, instr.operand[0], instr.operand[1],
//                            instrSet[instr.opcode].mnemonic, i + 1, gl_memory[instr.operand[0]], gl_memory[instr.operand[1]]);
        
//        if (counters) counter[i]++;
        i++;
    } while (i < numInstructions);
    
    // Result is stored in gl_memory[RI0 + numInstructions-1].
    return OK;
}
static inline int global_prgrm_simulate(const int startInstr) {
    
    if (startInstr > gl_program.length) {
        printf("Program simulation has gone horribly wrong. startInstr: %d\n", startInstr);
        return ERROR;
    }
    
    int i = startInstr;
    const int numInstructions = gl_program.length;
    
    do {
//        gl_memory[RI0 + i] = instr_simulate(instr, &gl_memory[0]);

        const Instruction instr = gl_program.instruction[i];
        
        gl_memory[RI0 + i] = (*instrSet[instr.opcode].proc)(gl_memory[instr.operand[0]], gl_memory[instr.operand[1]], 0);
        
//        print_to_console("%4s r%d, % d, % d, % d ) ===> %4s r%d, % d, % d, % d )\n",
//                            instrSet[instr.opcode].mnemonic, i + 1, instr.operand[0], instr.operand[1],
//                            instrSet[instr.opcode].mnemonic, i + 1, gl_memory[instr.operand[0]], gl_memory[instr.operand[1]]);

        i++;
    } while (i < numInstructions);
    
    // Result is stored in gl_memory[RI0 + numInstructions-1].
    return OK;
}




//static inline int prgrm_correct(const Program prog, const int startInstr, int* const gl_memory) {
static inline int prgrm_correct(const Program prog, const int startInstr) {

    int err;
    static int itrialx;          // Init 0.
    int kx;
#if NARGS >= 2
    static int itrialy;
    int ky;
#endif
#if NARGS >= 3
    static int itrialz;
    int kz;
#endif

    int corr_result;
    int numInstructions = prog.length;
    
    
#if NARGS == 1
    gl_memory[RX] = trialx[itrialx];
    corr_result = correct_result[itrialx];
#endif
#if NARGS == 2
    gl_memory[RX] = trialx[itrialx];
    gl_memory[RY] = trialy[itrialy];
    corr_result = correct_result[itrialx][itrialy];
#endif
#if NARGS == 3
    gl_memory[RX] = trialx[itrialx];
    gl_memory[RY] = trialy[itrialy];
    gl_memory[RZ] = trialz[itrialz];
    corr_result = correct_result[itrialx][itrialy][itrialz];
#endif


//    err = prgrm_simulate(prog, startInstr, gl_memory);
    err = prgrm_simulate(prog, startInstr);
    if (err == ERROR) {
        // Uh oh
        return ERROR;
    }
    if (gl_memory[RI0 + numInstructions-1] != corr_result) return FALSE;
    

    for (kx = 0; kx < NTRIALX - 1; kx++) {
        itrialx += 1;
        if (itrialx >= NTRIALX) itrialx = 0;
#if NARGS >= 2
        for (ky = 0; ky < NTRIALY - 1; ky++) {
            itrialy += 1;
            if (itrialy >= NTRIALY) itrialy = 0;
#endif
#if NARGS >= 3
            for (kz = 0; kz < NTRIALZ - 1; kz++) {
                itrialz += 1;
                if (itrialz >= NTRIALZ) itrialz = 0;
#endif

#if NARGS == 1
                gl_memory[RX] = trialx[itrialx];
                corr_result = correct_result[itrialx];
#endif
#if NARGS == 2
                gl_memory[RX] = trialx[itrialx];
                gl_memory[RY] = trialy[itrialy];
                corr_result = correct_result[itrialx][itrialy];
#endif
#if NARGS == 3
                gl_memory[RX] = trialx[itrialx];
                gl_memory[RY] = trialy[itrialy];
                gl_memory[RZ] = trialz[itrialz];
                corr_result = correct_result[itrialx][itrialy][itrialz];
#endif

                // Now we simulate the current program, i.e., the instructions
                // from 0 to numi-1.  The result of instruction i goes in
                // register i + RI0.

//                err = prgrm_simulate(prog, 0, gl_memory);
                err = prgrm_simulate(prog, 0);
                if (err == ERROR) {
                    // Uh oh
                    return ERROR;
                }
                if (gl_memory[RI0 + numInstructions-1] != corr_result) return FALSE;

#if NARGS >= 3
            }  // end kz
#endif
#if NARGS >= 2
        }  // end ky
#endif
    }  // end kx
    
    return TRUE;

}
static inline int global_prgrm_correct(const int startInstr) {

    int err;
    static int itrialx;          // Init 0.
    int kx;
#if NARGS >= 2
    static int itrialy;
    int ky;
#endif
#if NARGS >= 3
    static int itrialz;
    int kz;
#endif

    int corr_result;
    int numInstructions = gl_program.length;
    
    
#if NARGS == 1
    gl_memory[RX] = trialx[itrialx];
    corr_result = correct_result[itrialx];
#endif
#if NARGS == 2
    gl_memory[RX] = trialx[itrialx];
    gl_memory[RY] = trialy[itrialy];
    corr_result = correct_result[itrialx][itrialy];
#endif
#if NARGS == 3
    gl_memory[RX] = trialx[itrialx];
    gl_memory[RY] = trialy[itrialy];
    gl_memory[RZ] = trialz[itrialz];
    corr_result = correct_result[itrialx][itrialy][itrialz];
#endif


//    err = prgrm_simulate(prog, startInstr, gl_memory);
//    err = prgrm_simulate(gl_program, startInstr);
    err = global_prgrm_simulate(startInstr);
    if (err == ERROR) {
        // Uh oh
        return ERROR;
    }
    if (gl_memory[RI0 + numInstructions-1] != corr_result) return FALSE;
    

    for (kx = 0; kx < NTRIALX - 1; kx++) {
        itrialx += 1;
        if (itrialx >= NTRIALX) itrialx = 0;
#if NARGS >= 2
        for (ky = 0; ky < NTRIALY - 1; ky++) {
            itrialy += 1;
            if (itrialy >= NTRIALY) itrialy = 0;
#endif
#if NARGS >= 3
            for (kz = 0; kz < NTRIALZ - 1; kz++) {
                itrialz += 1;
                if (itrialz >= NTRIALZ) itrialz = 0;
#endif

#if NARGS == 1
                gl_memory[RX] = trialx[itrialx];
                corr_result = correct_result[itrialx];
#endif
#if NARGS == 2
                gl_memory[RX] = trialx[itrialx];
                gl_memory[RY] = trialy[itrialy];
                corr_result = correct_result[itrialx][itrialy];
#endif
#if NARGS == 3
                gl_memory[RX] = trialx[itrialx];
                gl_memory[RY] = trialy[itrialy];
                gl_memory[RZ] = trialz[itrialz];
                corr_result = correct_result[itrialx][itrialy][itrialz];
#endif

                // Now we simulate the current program, i.e., the instructions
                // from 0 to numi-1.  The result of instruction i goes in
                // register i + RI0.

//                err = prgrm_simulate(prog, 0, gl_memory);
                err = global_prgrm_simulate(0);
                if (err == ERROR) {
                    // Uh oh
                    return ERROR;
                }
                if (gl_memory[RI0 + numInstructions-1] != corr_result) return FALSE;

#if NARGS >= 3
            }  // end kz
#endif
#if NARGS >= 2
        }  // end ky
#endif
    }  // end kx
    
    return TRUE;

}




// Compute all the correct answers and save them in an array.
static inline void set_correct_results() {
    int i,j,k;
    for (i = 0; i < NTRIALX; i++) {
#if NARGS == 1
        correct_result[i] = userfun(trialx[i]);
#elif NARGS == 2
        for (j = 0; j < NTRIALY; j++)
            correct_result[i][j] = userfun(trialx[i], trialy[j]);
#elif NARGS == 3
        for (j = 0; j < NTRIALY; j++)
            for (k = 0; k < NTRIALZ; k++)
                correct_result[i][j][k] = userfun(trialx[i], trialy[j], trialz[k]);
#endif
    }
}


static inline int prgrm_search() {

    int valid, numInstructions, solutionCount;
//    int memory[NIM + NSHIM + NARGS + MAX_INSTRS] = {IMMEDS, SHIMMEDS};

    /* 
    printf("MEMORY_SIZE: %d. Register: ", MEMORY_SIZE);
    for(int x = 0; x < MEMORY_SIZE; x++) {
        printf("%d, ", gl_memory[x]);
    }
    printf("\n\n");
    */

    clock_t timeStart, timeFinish;

    set_correct_results();

    numInstructions = 1;
    solutionCount = 0;
    while (numInstructions <= MAX_INSTRS && solutionCount < 1) {
        
        progCount = 0;
        
        printf("\nTrying out programs with %d instructions.\n\n", numInstructions);
        
        timeStart = clock();
        
        int err = global_prgrm_init(numInstructions);
        
        if (err != ERROR) {        
            int lastModified = 0;
            do {
                valid = global_prgrm_correct(lastModified);
                if (valid == TRUE) {
                    global_prgrm_layer_set(0);
                    printf("P: %"PRIi64"\n", progCount);
                    prgrm_print(gl_program);
                    solutionCount++;
                }
                else if(valid == ERROR) {
                    return ERROR;
                }
                if (0) {
                    global_prgrm_layer_set(0);
                    prgrm_print(gl_program);
                }
                lastModified = global_prgrm_incr();
            } while (lastModified >= 0);
        }
        
        timeFinish = clock();
        
        print_to_console("Found %d solution(s). Went through %"PRIi64" programs. Time: %.3f\n\n", solutionCount, progCount, (double)(timeFinish-timeStart)/CLOCKS_PER_SEC);
        numInstructions++;
    }
    return 0;
}


int main() {
    return prgrm_search();
}