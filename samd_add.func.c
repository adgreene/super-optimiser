/* This is the function for which it is desired to find more efficient
code.  It must have either one or two arguments, both int, and must
return a 32-bit int quantity.  It is declared in aha.h. */
#ifndef FUNC_C
#define FUNC_C

#define NARGS 3

//#include "aha.h"

static inline int userfun(int a, int b, int mask) {
    int msb = (a ^ b) & mask;
    int sum = (a & ~mask) + (b & ~mask);
    return msb ^ sum;
}

#endif