/* program.h */
#ifndef PROGRAM_H
#define PROGRAM_H

#define MAX_OPRNDS 2
#define MAX_INSTRS 4

#define FALSE  0
#define TRUE   (!FALSE)

#define OK     0
#define ERROR -1

#include INC
#include "config.h"

typedef struct Instruction {
   int opcode;
   int operand[MAX_OPRNDS];
} Instruction;


typedef struct Program {
    int length;
    Instruction instruction[MAX_INSTRS];
    int layer[MAX_INSTRS];
} Program;



int immArr[] = { IMMEDS };
int shimmArr[] = { SHIMMEDS };

#define NIM ARR_LENGTH(immArr)
#define NSHIM ARR_LENGTH(shimmArr)

#define RX (NIM + NSHIM)        // First (or only) user function argument
#define RY (RX + 1)             // Second user function argument
#define RZ (RY + 1)             // Third user function argument
#define RI0 (RX + NARGS)        // Result of instruction 0 goes here

#define MEMORY_SIZE (NIM + NSHIM + NARGS + MAX_INSTRS)

#endif //PROGRAM_H