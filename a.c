#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "config.h"
//#include "search_set.h"
#include "print_funcs.h"
#include "simpleC.h"
#include "machine.h"
#include "program.h"


static inline int userfun1(int x, int y) {
    return ((unsigned long long)x + (unsigned long long)y) >> 1;
}

static inline int userfun2(int rx, int ry) {
    int r1,r2,r3,r4;
//    r1 = ry ^ rx;
//    r2 = ry & rx;
//    r3 = r1 >> 1;
//    r4 = r3 + r2;
    XOR( r1,ry,rx );
    AND( r2,ry,rx );
    SHR( r3,r1, 1 );
    ADD( r4,r3,r2 );
    return r4;
}

int main () {
    int a = 5;
    int b = 11;
    int c = 17;
    
//    printf("%d, %d\n", (30 >> 2), (-30 >> 2));
    
//    printf("%d, %d\n", userfun1(105, 720), userfun2(105, 720));

//    printf("%d\n", __builtin_popcount(b));

//    Instruction instr[2];
//    instr_init(&instr[0]);
//    instr_init(&instr[1]);
//    printf("%d %d %d %d\n", instr[1].operation, instr[1].operand[0], instr[1].operand[1], instr[1].operand[2]);

    int lastInstructionModified = 0;
    
    Program p;
    int out = prgrm_init(&p, 4);
    if (out != ERROR) {
        
        prgrm_print(p);
        
        int counter = 0;
        
        while (lastInstructionModified != -1) {
            lastInstructionModified = prgrm_incr(&p);
//            prgrm_print(p);
//            printf("Last mod: %d\n\n\n", lastInstructionModified);
            counter++;
        }
        
        printf("Programs generated: %d\n", counter);
    }
    
    return b;
}